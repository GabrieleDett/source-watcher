package gabriele.sourcewatcher;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import io.github.eliseomartelli.simplecustomtabs.CustomTabs;

public class SourceViewerActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String url = "view-source:" + getIntent().getStringExtra(Intent.EXTRA_TEXT);
        CustomTabs.with(this).warm();
        CustomTabs.with(this)
                .setStyle(new CustomTabs.Style(this)
                        .setToolbarColor(R.color.colorPrimary))
               /
    }
}
